# SHSCVI_Individual_1415_P4

Detección de líneas a lo largo de direcciones principales. Detectar los bordes
de la imagen “bridge.jpg”. Utilizad las derivadas direccionales de primer orden
para detectar primero los bordes horizontales, luego los verticales y componer
en último lugar la imagen formada de bordes horizontales y verticales.
Discutid los resultados obtenidos utilizando el filtro de Sobel o el de Prewitt.
Alternativamente utilizad el filtro de Canny y discutir los resultados.
Analizar los resultados en función de diferentes valores elegidos para los 
umbrales (inferior y superior) y de los efectos de histéresis resultantes. 
Realizad una valoración de los resultados obtenidos.
Nota: como extra, se ha decidido incluir el filtro de Scharr para realizar
una comparativa entre los cuatro filtros.


Sistemas Hardware y Software de Captura y Visualización de Imagen, 
Máster en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.


# Autor

Tejedor García,		Cristian.


# Estructura de directorios:
/doc/
Documentación del proyecto

/src/
Código fuente de la aplicación

/img/
Imágenes de prueba de la aplicación

/resultados/
Imágenes y pdfs obtenidos tras ejecutar
la aplicación


# Intérprete de Python utilizado
2.7.X
https://www.python.org/downloads/


# Versión de OpenCV utilizada
3.0.0-alpha
http://sourceforge.net/projects/opencvlibrary/files/


# Dependencias
Numpy 1.8.2 http://sourceforge.net/projects/numpy/files/NumPy/

Matplotlib 1.4.2 http://matplotlib.org/downloads.html


# Clase principal
src/Practica4.py
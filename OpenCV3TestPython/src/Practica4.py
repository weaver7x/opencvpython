# -*- coding: UTF-8 -*-

'''
Created on 10/12/2014

Práctica 4
Detección de líneas a lo largo de direcciones principales.
Comparativa visual de los filtros de Sobel, Scharr, Prewitt y Canny.

Utiliza OpenCV 3.0.0-alpha, matplotlib y numpy

@author: Cristian TG
@version: 1.0-INITIAL
@since: 0.1-ALPHA
'''

import cv2
from matplotlib import pyplot as plt
import numpy as np


def filtros(ruta):
    '''
    Aplica diferentes filtros a la imagen indicada.
    Muestra los tiempos de procesado.
    ruta: ruta de la imagen a aplicar filtros.
    '''
    
    # Paso 0: Leer imagen, y pasar a escala de grises
    gray = cv2.cvtColor(cv2.imread(ruta, 1), cv2.COLOR_BGR2GRAY)


    # Paso 1: Scharr y Sobel utilizando kernel=Scharr(-1) y Sobel kernel=1
    plt.figure().canvas.set_window_title('Paso 1: Scharr y Sobel utilizando kernel=Scharr(-1) y Sobel kernel=1')
    
    # 1.1 Scharr
    # dX: Extraer segmentos verticales
    scharrDx = cv2.Scharr(gray, cv2.CV_64F, 1, 0)
    scharrDx = cv2.convertScaleAbs(scharrDx)
    # dY: Extraer segmentos horizontales
    scharrDy = cv2.Scharr(gray, cv2.CV_64F, 0, 1)
    scharrDy = cv2.convertScaleAbs(scharrDy)
    # Suma
    scharrDxDy = cv2.addWeighted(scharrDx, 0.5, scharrDy, 0.5, 0)
    
    # 1.2 Sobel (-1 es el kernel especial de Scharr)
    # dX: Extraer segmentos verticales
    sobelDx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=-1)
    sobelDx = cv2.convertScaleAbs(sobelDx)
    # dY: Extraer segmentos horizontales
    sobelDy = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=-1)
    sobelDy = cv2.convertScaleAbs(sobelDy)
    # Suma
    sobelDxDy = cv2.addWeighted(sobelDx, 0.5, sobelDy, 0.5, 0)
        
    # 1.3 Sobel kernel 1
    # dX: Extraer segmentos verticales
    sobelDx1 = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=1)
    sobelDx1 = cv2.convertScaleAbs(sobelDx1)
    # dY: Extraer segmentos horizontales
    sobelDy1 = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=1)
    sobelDy1 = cv2.convertScaleAbs(sobelDy1)
    # Suma
    sobelDxDy1 = cv2.addWeighted(sobelDx1, 0.5, sobelDy1, 0.5, 0)
    
    # Mostrar resultados del paso 1
    titles = ['Scharr dX', 'Scharr dY', 'Scharr Ambas', 'Sobel dX k=Scharr', 'Sobel dY k=Scharr', 'Sobel Ambas k=Scharr', 'Sobel dX k=1', 'Sobel dY k=1', 'Sobel Ambas k=1' ]
    images = [scharrDx, scharrDy, scharrDxDy, sobelDx, sobelDy, sobelDxDy, sobelDx1, sobelDy1, sobelDxDy1]
    for i in xrange(int(len(images))):
        plt.subplot(3, 3, i + 1)
        plt.imshow(images[i], 'gray')
        plt.title(titles[i])
        plt.xticks([]), plt.yticks([])
    plt.show()


    # Paso 2: Prewitt, Sobel y Canny kernel=3
    plt.figure().canvas.set_window_title('Paso 2: Prewitt, Sobel y Canny kernel=3')
    
    # 2.1 Prewitt kernel 3
    # dX: Extraer segmentos verticales
    k = np.array([ [-1, 0, 1], [-1, 0, 1], [-1, 0, 1] ], np.float32)
    prewittDx = cv2.filter2D(gray, -1, k)
    # dY: Extraer segmentos horizontales
    k = np.transpose(k)
    prewittDy = cv2.filter2D(gray, -1, k)
    # Suma
    prewittDxDy = cv2.addWeighted(prewittDx, 0.5, prewittDy, 0.5, 0)
    
    # 2.2 Sobel kernel 3
    # dX: Extraer segmentos verticales
    sobelDx3 = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=3)
    sobelDx3 = cv2.convertScaleAbs(sobelDx3)
    # dY: Extraer segmentos horizontales
    sobelDy3 = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=3)
    sobelDy3 = cv2.convertScaleAbs(sobelDy3)
    # Suma
    sobelDxDy3 = cv2.addWeighted(sobelDx3, 0.5, sobelDy3, 0.5, 0)
    
    # 2.3 Canny
    grayCanny3 = cv2.GaussianBlur(gray, (3, 3), 0)
    canny3 = cv2.Canny(grayCanny3, 75, 150, 3)
    grayCanny5 = cv2.GaussianBlur(gray, (5, 5), 0)
    canny5 = cv2.Canny(grayCanny5, 75, 150, 5)
    grayCanny7 = cv2.GaussianBlur(gray, (3, 3), 0)
    canny7 = cv2.Canny(grayCanny7, 75, 150, 7)
    
    # Mostrar resultados del paso 2
    titles = ['Prewitt Ambas k=3', 'Prewitt dX k=3', 'Prewitt dY k=3', 'Sobel Ambas k=3', 'Sobel dX k=3', 'Sobel dY k=3', 'Canny k=3', 'Canny k=5', 'Canny k=7']
    images = [prewittDxDy, prewittDx, prewittDy, sobelDxDy3, sobelDx3, sobelDy3, canny3, canny5, canny7]
    for i in xrange(int(len(images))):
        plt.subplot(3, 3, i + 1)
        plt.imshow(images[i], 'gray')
        plt.title(titles[i])
        plt.xticks([]), plt.yticks([])
    plt.show()


    # Paso 3: Canny con distintos varlores de histéresis
    plt.figure().canvas.set_window_title('Paso 3: Canny k=5 con distintos varlores de histéresis')
                
    cannyA = cv2.Canny(grayCanny5, 0, 50, 5)
    cannyB = cv2.Canny(grayCanny5, 50, 100, 5)
    cannyC = cv2.Canny(grayCanny5, 100, 150, 5)
    cannyD = cv2.Canny(grayCanny5, 150, 200, 5)
    cannyE = cv2.Canny(grayCanny5, 100, 200, 5)
    cannyF = cv2.Canny(grayCanny5, 0, 100, 5)
    cannyG = cv2.Canny(grayCanny5, 0, 150, 5)
    cannyH = cv2.Canny(grayCanny5, 0, 200, 5)
    cannyI = cv2.Canny(grayCanny5, 100, 100, 5)
    
    # Mostrar resultados del paso 3
    titles = ['Canny 0-50', 'Canny 50-100', 'Canny 100-150', 'Canny 150-200', 'Canny 100-200', 'Canny 0-100', 'Canny 0-150', 'Canny 0-200', 'Canny 100-100']
    images = [cannyA, cannyB, cannyC, cannyD, cannyE, cannyF, cannyG, cannyH, cannyI]
    for i in xrange(int(len(images))):
        plt.subplot(3, 3, i + 1)
        plt.imshow(images[i], 'gray')
        plt.title(titles[i])
        plt.xticks([]), plt.yticks([])    
    plt.show()

    return None



filtros('../img/bridge.jpg')
filtros('../img/mujer.jpg')
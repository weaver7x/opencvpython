# -*- coding: UTF-8 -*-

'''
Created on 10/12/2014

Práctica 5
Algoritmo de Roberts.
Comparativa visual y mediante tiempos de filtros a una imagen: Roberts, Canny, Prewitt, Sobel y Scharr.

Utiliza OpenCV 3.0.0-alpha, matplotlib y numpy

@author: Cristian TG
@version: 1.0-INITIAL
@since: 0.1-ALPHA
'''

import cv2
from matplotlib import pyplot as plt
import numpy as np, datetime


def filtros(ruta):
    '''
    Aplica diferentes filtros a la imagen indicada.
    Muestra los tiempos de procesado.
    ruta: ruta de la imagen a aplicar filtros.
    '''
    plt.figure().canvas.set_window_title('Filtros y tiempos de la imagen')
    imgBGR = cv2.imread(ruta, 1)
    b, g, r = cv2.split(imgBGR)    
    imgRGB = cv2.merge((r, g, b))
    gray = cv2.cvtColor(imgBGR, cv2.COLOR_BGR2GRAY)
    
    # Roberts
    tic = datetime.datetime.now().microsecond
    k = np.array([ [-1, 0], [0, 1]], np.float32)
    grayXR = cv2.filter2D(gray, -1, k)
    k = np.transpose(k)
    grayYR = cv2.filter2D(gray, -1, k)
    roberts = cv2.add(grayXR, grayYR)    
    toc = datetime.datetime.now().microsecond
    robertsT = toc - tic
    
    # Canny
    tic = datetime.datetime.now().microsecond
    canny = cv2.Canny(gray, 75, 150)
    toc = datetime.datetime.now().microsecond
    cannyT = toc - tic
    
    # Prewitt
    tic = datetime.datetime.now().microsecond
    k = np.array([ [-1, 0, 1], [-1, 0, 1], [-1, 0, 1] ], np.float32)
    prewittDx = cv2.filter2D(gray, -1, k)
    k = np.transpose(k)
    prewittDy = cv2.filter2D(gray, -1, k)
    prewitt = cv2.addWeighted(prewittDx, 0.5, prewittDy, 0.5, 0)
    toc = datetime.datetime.now().microsecond
    prewittT = toc - tic    
    
    # Sobel
    tic = datetime.datetime.now().microsecond
    sobelDx = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=3)
    sobelDx = cv2.convertScaleAbs(sobelDx)
    sobelDy = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=3)
    sobelDy = cv2.convertScaleAbs(sobelDy)
    sobel = cv2.addWeighted(sobelDx, 0.5, sobelDy, 0.5, 0)
    toc = datetime.datetime.now().microsecond
    sobelT = toc - tic
    
    # Scharr
    tic = datetime.datetime.now().microsecond
    scharrDx = cv2.Scharr(gray, cv2.CV_32F, 1, 0)
    scharrDx = cv2.convertScaleAbs(scharrDx)
    scharrDy = cv2.Scharr(gray, cv2.CV_32F, 0, 1)
    scharrDy = cv2.convertScaleAbs(scharrDy)
    scharr = cv2.addWeighted(scharrDx, 0.5, scharrDy, 0.5, 0)
    toc = datetime.datetime.now().microsecond
    scharrT = toc - tic
    
    # Mostrar imágenes y tiempos del resultado
    titles = ['Original', 'Roberts: ' + str(robertsT) + ' microsegundos', 'Canny: ' + str(cannyT) + ' microsegundos', 'Prewitt: ' + str(prewittT) + ' microsegundos', 'Sobel: ' + str(sobelT) + ' microsegundos', 'Scharr: ' + str(scharrT) + ' microsegundos']
    images = [imgRGB, roberts, canny, prewitt, sobel, scharr]
    for i in xrange(int(len(images))):
        plt.subplot(2, 3, i + 1)
        plt.imshow(images[i], 'gray')
        plt.title(titles[i])
        plt.xticks([]), plt.yticks([])
    plt.show()
    
    return None


filtros('../img/fachada1.jpg')
filtros('../img/fachada2.jpg')

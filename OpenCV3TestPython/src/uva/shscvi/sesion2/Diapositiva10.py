# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 10
Matrices de transformación afín y perspectiva
Created on 28/10/2014

@author: Cristian TG
'''

import numpy as np
import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

# RGB es como trabaja por defecto MatPlotLib
img = FuncionesComunes.imgRGB('coche.png', 1)


rows, cols, channels = img.shape

# Matriz de transformación afín (3 puntos)
pts1 = np.float32([[50, 50], [200, 50], [50, 200]])
pts2 = np.float32([[10, 100], [200, 50], [100, 250]])
M = cv2.getAffineTransform(pts1, pts2)
afin = cv2.warpAffine(img, M, (cols, rows))
# Mostrar
plt.subplot(1, 2, 1)
plt.imshow(afin, cmap='gray', interpolation='bicubic')
plt.title('Afin (3 puntos)')
plt.xticks([]), plt.yticks([])


# Matriz de transformación en perspectiva (4 puntos)
pts1 = np.float32([[56, 65], [368, 52], [28, 387], [389, 390]])
pts2 = np.float32([[0, 0], [300, 0], [0, 300], [300, 300]])
M = cv2.getPerspectiveTransform(pts1, pts2)
pers = cv2.warpPerspective(img, M, (cols, rows))
# Mostrar
plt.subplot(1, 2, 2)
plt.imshow(pers, cmap='gray', interpolation='bicubic')
plt.xticks([]), plt.yticks([])
plt.title('Perspectiva (4 puntos)')

plt.show()

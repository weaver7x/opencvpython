# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 8
Transformaciones geométricas: escala, rotar, trasladar
Created on 28/10/2014

@author: Cristian TG
'''

import numpy as np
import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

imgBGR = FuncionesComunes.imgBGR('coche.png', 1)
imgRGB = FuncionesComunes.imgRGB('coche.png', 1)

plt.subplot(2, 3, 1)
plt.imshow(imgBGR, cmap='gray', interpolation='bicubic')
plt.title("BGR (por defecto)")
plt.xticks([]), plt.yticks([])

plt.subplot(2, 3, 2)
plt.imshow(imgRGB, cmap='gray', interpolation='bicubic')
plt.title("RGB")
plt.xticks([]), plt.yticks([])



rows, cols, channels = imgRGB.shape

res = cv2.resize(imgRGB, (100, 100), interpolation=cv2.INTER_CUBIC)
plt.subplot(2, 3, 3)
plt.imshow(res, cmap='gray', interpolation='bicubic')
plt.title("Escala 100x100 pixeles (afin)")
plt.xticks([]), plt.yticks([])


M = np.float32([[1, 0, 150], [0, 1, 80]])  # 150 pixeles a la derecha y 80 hacia abajo
trans = cv2.warpAffine(imgRGB, M, (cols, rows))
plt.subplot(2, 3, 4)
plt.imshow(trans, cmap='gray', interpolation='bicubic')
plt.title("Trasladar (afin)")
plt.xticks([]), plt.yticks([])


M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 90, 1)
rot = cv2.warpAffine(imgRGB, M, (cols, rows))
plt.subplot(2, 3, 5)
plt.imshow(rot, cmap='gray', interpolation='bicubic')
plt.title("Rotar (afin)")
plt.xticks([]), plt.yticks([])

plt.show()

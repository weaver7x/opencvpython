# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 15
Filtrado (Suavizado) (Convolución 2D) (Smoothing Images)
# Más ejemplos en:
# http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_filtering/py_filtering.html
Created on 28/10/2014

@author: Cristian TG
'''

import numpy as np
import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

img = FuncionesComunes.imgRGB('coche.png', 1)

plt.subplot(2, 2, 1)
plt.imshow(img, cmap='gray', interpolation='bicubic')
plt.title("Original")
plt.xticks([]), plt.yticks([])

kernel = np.ones((5, 5), np.float32) / 25
dst = cv2.filter2D(img, -1, kernel)

plt.subplot(2, 2, 2)
plt.imshow(dst, cmap='gray', interpolation='bicubic')
plt.title("Filtrado filter2D 5x5")
plt.xticks([]), plt.yticks([])


blur = cv2.blur(img, (5, 5))

plt.subplot(2, 2, 3)
plt.imshow(blur, cmap='gray', interpolation='bicubic')
plt.title("Filtrado 5x5 blur")
plt.xticks([]), plt.yticks([])

gBlur = cv2.medianBlur(img, 5)

plt.subplot(2, 2, 4)
plt.imshow(gBlur, cmap='gray', interpolation='bicubic')
plt.title("Filtrado 5x5 MedianBlur")
plt.xticks([]), plt.yticks([])

plt.show()

# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 5
Leer y mostrar imagen
Created on 28/10/2014

@author: Cristian TG
'''

import cv2
from cv2 import waitKey

# OpenCV imread lee la imagen en BGR
# 0 en escala de grises, 1 en color sin transparencia, -1 en color con transparencia
imgBGR = cv2.imread("./coche.png", -1)
cv2.imshow("Coche", imgBGR)
waitKey(0)  # Para mostrar la ventana
cv2.destroyAllWindows()  # Destruye todas las ventanas creadas
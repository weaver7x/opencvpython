# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 7
Operaciones básicas sobre imágenes
Created on 28/10/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
from cv2 import waitKey

# BGR es como trabaja por defecto OpenCV
img = FuncionesComunes.imgRGB('coche.png', 1)

print 'shape {filas(alto), columnas(ancho)}', img.shape  # filas(alto), columnas(ancho)
a, b, c = img.shape
print ''
print 'a ', a
print 'b ', b
print 'c ', c
print

print 'size', img.size  #  número de píxels
print

# Forma 1 de acceder a píxeles: inefeciente
print 'Color del píxel [fila:100,columna:100] = ', img[100, 100]  # Color del píxel [fila:100,columna:100] Coste computacional
# img[109,109] = [0,0,0] # Hacer negro el pixel
print

# Forma 2 de acceder a píxeles: eficiente
rgb = (0, 1, 2)
print 'componentes: ', rgb
for componente in rgb:
    # img.itemset((100, 100, componente),4) # Cambia el valor del canal a 4. 
    print 'Color del píxel [fila:100,columna:100, canal:componente(i)]', img.item(100, 100, componente)  # Color del píxel [fila:100,columna:100, canal:componente]
    
print img.dtype  # Tipo de datos de la imagen, importante para evitar errores en OpenCV

# ROI copiar una región de imagen y pegarla
eye = img[280:340, 330:390]
imgROI = img.copy()
imgROI[273:333, 100:160] = eye
cv2.imshow("Coche con copia de 60x60", imgROI)
# waitKey(0)  # Para mostrar la ventana
# cv2.destroyAllWindows()  # Destruye todas las ventanas creadas


# Seleccionar el canal azul: blue = img[:,:,0]
print img[:, :, 0]
# Eliminar el canal rojo (BGR el tercero es 2) de la imagen: img[:,:,2] = 0
img[:, :, 2] = 0
cv2.imshow("Coche sin rojo", img)
waitKey(0)  # Para mostrar la ventana
cv2.destroyAllWindows()  # Destruye todas las ventanas creadas

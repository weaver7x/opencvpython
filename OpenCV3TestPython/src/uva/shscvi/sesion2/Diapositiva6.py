# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 6
Leer y mostrar imagen Matplotlib
Created on 28/10/2014

@author: Cristian TG
'''

import cv2
from matplotlib import pyplot as plt
# Esta biblioteca de Python permite visualizar imágenes y dibujar plots
# No necesita waitKey()
# OpenCV guarda imágenes BGR pero Matplotlib lee imágenes RGB

imgBGR = cv2.imread('coche.png', 1)  # BGR
# OpenCV guarda las imágenes en BGR, para transformarlo split() gran coste computacional
b, g, r = cv2.split(imgBGR)
# Imagen leida en RGB con merge
imgRGB = cv2.merge((r, g, b))

# Mostrar imagen BGR
plt.subplot(1, 2, 1)
plt.imshow(imgBGR, cmap='gray', interpolation='bicubic')
# oculta valores en los ejes
plt.xticks([]), plt.yticks([])
plt.title("Coche BGR")

# Mostrar imagen RGB
plt.subplot(1, 2, 2)
plt.imshow(imgRGB, cmap='gray', interpolation='bicubic')
# oculta valores en los ejes
plt.xticks([]), plt.yticks([])
plt.title("Coche RGB")
plt.show()

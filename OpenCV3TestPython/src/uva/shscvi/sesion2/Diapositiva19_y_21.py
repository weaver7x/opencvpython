# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 19 y Diapositiva 21
19: Detección de segmentos: Sobel, Scharr y Laplaciano.
21: Detección de segmentos: Canny (están desconectados, en contornos unidos).
Extra: Prewitt y Roberts (por Cristian TG)
# Más ejemplos en:
# http://docs.opencv.org/trunk/doc/py_tutorials/py_grayproc/py_gradients/py_gradients.html
Created on 28/10/2014

@author: Cristian TG
'''
 
import cv2
from matplotlib import pyplot as plt
import numpy as np

# Opción 1 -> A escala de grises
gray = cv2.cvtColor(cv2.imread('coche.png', 1), cv2.COLOR_BGR2GRAY)
# Opción 2
# gray = cv2.imread('coche.png', 1)

plt.subplot(4, 3, 1)  # filas, columnas, índice de celda
plt.imshow(gray, cmap='gray', interpolation='bicubic')
plt.title("Original")
plt.xticks([]), plt.yticks([])

######### Laplaciano
laplacian = cv2.Laplacian(gray, cv2.CV_16U)
plt.subplot(4, 3, 2)  # filas, columnas, índice de celda
plt.imshow(laplacian, cmap='gray', interpolation='bicubic')
plt.title("Laplaciano 5x5")
plt.xticks([]), plt.yticks([])

######### Sobel X
sobelx = cv2.Sobel(gray, cv2.CV_16U, 1, 0, ksize=5)
plt.subplot(4, 3, 4)  # filas, columnas, índice de celda
plt.imshow(sobelx, cmap='gray', interpolation='bicubic')
plt.title("SobelX 5x5")
plt.xticks([]), plt.yticks([])

######### Sobel Y
sobely = cv2.Sobel(gray, cv2.CV_16U, 0, 1, ksize=5)
plt.subplot(4, 3, 5)  # filas, columnas, índice de celda
plt.imshow(sobely, cmap='gray', interpolation='bicubic')
plt.title("SobelY 5x5")
plt.xticks([]), plt.yticks([])

suma = cv2.add(sobelx, sobely)
plt.subplot(4, 3, 6)  # filas, columnas, índice de celda
plt.imshow(suma, cmap='gray', interpolation='bicubic')
plt.title("Suma Sobel X e Y")
plt.xticks([]), plt.yticks([])



######### Canny
edges = cv2.Canny(gray, 100, 200)  # 100 es el mínimo y 200 el máximo para la histéresis
plt.subplot(4, 3, 3)  # filas, columnas, índice de celda
plt.imshow(edges, cmap='gray', interpolation='bicubic')
plt.title("Canny")
plt.xticks([]), plt.yticks([])




######## Prewitt
# Extraer segmentos verticales (hecho por el profesor)
grayX = cv2.cvtColor(cv2.imread('coche.png', 1), cv2.COLOR_BGR2GRAY)
k = np.array([ [-1, 0, 1], [-1, 0, 1], [-1, 0, 1] ], np.float32)
grayX = cv2.filter2D(grayX, -1, k)
plt.subplot(4, 3, 7)  # filas, columnas, índice de celda
plt.imshow(grayX, cmap='gray', interpolation='bicubic')
plt.title("Prewitt ext. vertical (derivada en X)")
plt.xticks([]), plt.yticks([])


# Extraer segmentos horizontales (hecho por Cristian TG)
grayY = cv2.cvtColor(cv2.imread('coche.png', 1), cv2.COLOR_BGR2GRAY)
k = np.array([ [-1, -1, -1], [0, 0, 0], [1, 1, 1] ], np.float32)
grayY = cv2.filter2D(grayY, -1, k)
plt.subplot(4, 3, 8)  # filas, columnas, índice de celda
plt.imshow(grayY, cmap='gray', interpolation='bicubic')
plt.title("Prewitt ext. horizontal (derivada en Y)")
plt.xticks([]), plt.yticks([])

suma = cv2.add(grayX, grayY)
plt.subplot(4, 3, 9)  # filas, columnas, índice de celda
plt.imshow(suma, cmap='gray', interpolation='bicubic')
plt.title("Suma Prewitt X e Y")
plt.xticks([]), plt.yticks([])


######## Roberts
# Extraer segmentos verticales (hecho por Cristian TG)
grayXR = cv2.cvtColor(cv2.imread('coche.png', 1), cv2.COLOR_BGR2GRAY)
k = np.array([ [-1, 0, 0], [0, 1, 0], [0, 0, 0] ], np.float32)
grayXR = cv2.filter2D(grayXR, -1, k)
plt.subplot(4, 3, 10)  # filas, columnas, índice de celda
plt.imshow(grayXR, cmap='gray', interpolation='bicubic')
plt.title("Roberts ext. vertical columnas (derivada en X)")
plt.xticks([]), plt.yticks([])

# Extraer segmentos horizontales (hecho por Cristian TG)
grayYR = cv2.cvtColor(cv2.imread('coche.png', 1), cv2.COLOR_BGR2GRAY)
k = np.array([ [0, 0, 0], [0, 0, 1], [0, -1, 0] ], np.float32)
grayYR = cv2.filter2D(grayYR, -1, k)
plt.subplot(4, 3, 11)  # filas, columnas, índice de celda
plt.imshow(grayYR, cmap='gray', interpolation='bicubic')
plt.title("Roberts ext. horizontal filas (derivada en Y)")
plt.xticks([]), plt.yticks([])

suma = cv2.add(grayXR, grayYR)
plt.subplot(4, 3, 12)  # filas, columnas, índice de celda
plt.imshow(suma, cmap='gray', interpolation='bicubic')
plt.title("Suma Roberts X e Y")
plt.xticks([]), plt.yticks([])


plt.show()
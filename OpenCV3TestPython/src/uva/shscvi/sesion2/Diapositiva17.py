# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 17
Operadores morfológicos (normalmente en imágenes binarias)
# Más ejemplos en:
# http://docs.opencv.org/trunk/doc/py_tutorials/py_imgproc/py_morphological_ops/py_morphological_ops.html
Created on 28/10/2014

@author: Cristian TG
'''

import numpy as np
import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

img = FuncionesComunes.imgRGB('coche.png', 1)

# kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))
kernel = np.ones((7, 7), np.uint8)  # 8 bits sin signo
closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

plt.subplot(2, 3, 3)  # filas, cloumnas, índice de celda
plt.imshow(closing, cmap='gray', interpolation='bicubic')
plt.title("Closing (dilatacion + erosion) 7x7")
plt.xticks([]), plt.yticks([])

########################################################
kernel2 = np.ones((20, 20), np.uint8)  # 8 bits sin signo
closing2 = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel2)

plt.subplot(2, 3, 2)  # filas, cloumnas, índice de celda
plt.imshow(closing2, cmap='gray', interpolation='bicubic')
plt.title("Closing (dilatacion + erosion) Kernel 20x20")
plt.xticks([]), plt.yticks([])


########################################################
kernel3 = cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))
closing3 = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel3)

plt.subplot(2, 3, 4)  # filas, cloumnas, índice de celda
plt.imshow(closing3, cmap='gray', interpolation='bicubic')
plt.title("Closing (dilatacion + erosion) Structuring MORPH 5x5")
plt.xticks([]), plt.yticks([])


########################################################
kernel4 = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
erode = cv2.erode(img, kernel4)

plt.subplot(2, 3, 5)  # filas, cloumnas, índice de celda
plt.imshow(erode, cmap='gray', interpolation='bicubic')
plt.title("Erosion RECT 7x7")
plt.xticks([]), plt.yticks([])


########################################################
kernel5 = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
dilate = cv2.dilate(img, kernel5)

plt.subplot(2, 3, 6)  # filas, cloumnas, índice de celda
plt.imshow(dilate, cmap='gray', interpolation='bicubic')
plt.title("Dilatacion RECT 7x7")
plt.xticks([]), plt.yticks([])


########################################################
plt.subplot(2, 3, 1)  # filas, cloumnas, índice de celda
plt.imshow(img, cmap='gray', interpolation='bicubic')
plt.title("Original")
plt.xticks([]), plt.yticks([])


plt.show()

# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 12
Umbralizado (pasar la imagen a un canal siempre = Ig (escala de grises) antes)
Created on 28/10/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

# Pasar de BGR a escala de grises (1 canal) Ig
img2 = cv2.cvtColor(FuncionesComunes.imgBGR('coche.png', 1), cv2.COLOR_BGR2GRAY)

# Aplicar el umbral (cuando el pixel sea mayor que 127, se pone a 255) 
t, ret = cv2.threshold(img2, 127, 255, cv2.THRESH_BINARY)


plt.imshow(ret, cmap='gray', interpolation='bicubic')
plt.title("Umbralizado (binarizacion)")
plt.xticks([]), plt.yticks([])


plt.show()
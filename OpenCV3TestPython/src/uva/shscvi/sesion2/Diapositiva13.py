# -*- coding: UTF-8 -*-

'''
Sesión 2
Diapositiva 13
Umbralizado adaptativo (pasar la imagen a un canal siempre antes)
Created on 28/10/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

# Pasar de color a grey
img = cv2.cvtColor(FuncionesComunes.imgBGR('coche.png', 1), cv2.COLOR_BGR2GRAY)

# Opción 1
ret = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
plt.subplot(1, 2, 1)
plt.imshow(ret, cmap='gray', interpolation='bicubic')
plt.title("Umbral adaptativo MEAN")
plt.xticks([]), plt.yticks([])

# Opción 2
ret2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
plt.subplot(1, 2, 2)
plt.imshow(ret2, cmap='gray', interpolation='bicubic')
plt.title("Umbral adaptativo GAUSSIAN")
plt.xticks([]), plt.yticks([])


plt.show()
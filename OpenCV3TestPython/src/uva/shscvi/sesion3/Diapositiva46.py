# -*- coding: UTF-8 -*-

'''
Sesión 3
Diapositiva 46
Vídeo
Detección de puntos característicos

Created on 11/11/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
import numpy as np
from matplotlib import pyplot as plt

# Opción 1
img = FuncionesComunes.imgRGB('lena.jpg', 1)
gray = FuncionesComunes.imgBGR('lena.jpg', 0)
corners = cv2.goodFeaturesToTrack(gray, 100, 0.01, 10)
corners = np.int0(corners)
for i in corners:
    x, y = i.ravel()
    cv2.circle(img, (x, y), 10, (0, 255, 0), 1)
    # ultimo parametro con -1 el circulo esta relleno, con 1 esta vacio
    # cv2.circle(img, (x, y), 3, 255, -1)


plt.subplot(1, 1, 1)  # filas, cloumnas, índice de celda
plt.imshow(img, cmap='gray', interpolation='bicubic')
plt.title("Contornos")
plt.xticks([]), plt.yticks([])

plt.show()

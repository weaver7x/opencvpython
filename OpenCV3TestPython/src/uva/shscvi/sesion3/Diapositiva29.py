# -*- coding: UTF-8 -*-

'''
Sesión 3
Diapositiva 29
Ecualización
Clahe

Created on 11/11/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

# Opción 1 - Escala de grises
gray = FuncionesComunes.imgBGR('lena.jpg', 0)

plt.subplot(2, 3, 1)  # filas, cloumnas, índice de celda
plt.imshow(gray, cmap='gray', interpolation='bicubic')
plt.title("Grises")
plt.xticks([]), plt.yticks([])

plt.subplot(2, 3, 4)  # filas, cloumnas, índice de celda
plt.hist(gray.ravel(), 256, [0, 256])
plt.title("Grises Histograma")
plt.xticks([]), plt.yticks([])


#################### EQUALIZE

img_equ = cv2.equalizeHist(gray)
plt.subplot(2, 3, 2)  # filas, cloumnas, índice de celda
plt.imshow(img_equ, cmap='gray', interpolation='bicubic')
plt.title("Ecualizacion")
plt.xticks([]), plt.yticks([])

plt.subplot(2, 3, 5)  # filas, cloumnas, índice de celda
plt.hist(img_equ.ravel(), 256, [0, 256])
plt.title("Equal Histograma")
plt.xticks([]), plt.yticks([])


#################### CLAHE

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
img_equ_clahe = clahe.apply(gray)
plt.subplot(2, 3, 3)  # filas, cloumnas, índice de celda
plt.imshow(img_equ_clahe, cmap='gray', interpolation='bicubic')
plt.title("Clahe")
plt.xticks([]), plt.yticks([])

plt.subplot(2, 3, 6)  # filas, cloumnas, índice de celda
plt.hist(img_equ_clahe.ravel(), 256, [0, 256])
plt.title("Clahe Histograma")
plt.xticks([]), plt.yticks([])

plt.show()

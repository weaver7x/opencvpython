# -*- coding: UTF-8 -*-

'''
Sesión 3
Diapositiva 26 y 27
Histogramas: en grises y en color
# http://docs.opencv.org/trunk/doc/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html
Created on 11/11/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
from matplotlib import pyplot as plt


ruta = 'lena.jpg'
# Opcion 1:Siempre en escala de grises
grises = FuncionesComunes.imgBGR(ruta, 0)
plt.hist(grises.ravel(), 256, [0, 256])
plt.title("Grises")
plt.show()

orig = FuncionesComunes.imgBGR(ruta, 1)

# El de HSV no se va  ausar, no hacerlo
# hsv = cv2.cvtColor(orig, cv2.COLOR_BGR2HSV)
# cv2.calcHist(hsv, [0, 1], None, [180, 256], 2 [0, 180, 0, 256])
# print data
# plt.subplot(1, 1, 1)  # filas, cloumnas, índice de celda
# plt.imshow(image, cmap='gray', interpolation='bicubic')
# plt.title("Histograma")
# plt.xticks([]), plt.yticks([])

# Opción 2 Color, uno a uno
#
color = ('b', 'g', 'r')
for i, col in enumerate(color):
    histr = cv2.calcHist([orig], [i], None, [256], [0, 256])
    plt.plot(histr, color=col)
    plt.xlim([0, 256])
plt.title("Color")
plt.show()
# -*- coding: UTF-8 -*-

'''
Sesión 3
Diapositivas 23 y 24
Contornos (curva que une puntos contiguos de los bordes)

Created on 11/11/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
from matplotlib import pyplot as plt

# Opción 1
orig = FuncionesComunes.imgBGR('lena.jpg', 1)
img = cv2.cvtColor(orig, cv2.COLOR_BGR2GRAY)

# Opción 2 - Escala de grises
# img = cv2.imread('coche.png', 0)

# 1 Canny
# Opcional
img = cv2.GaussianBlur(img, (5, 5), 0)
# Obligatorio
edges = cv2.Canny(img, 50, 200)  # 100 mínimo y 200 máximo para la histeresis
plt.subplot(1, 2, 1)  # filas, cloumnas, índice de celda
plt.imshow(edges, cmap='gray', interpolation='bicubic')
plt.title("Canny")
plt.xticks([]), plt.yticks([])

# 2 Contornos
# Lectura Se necesita el resultado de Canny
img, contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# Escritura sobre la original
image = cv2.drawContours(orig, contours, -1, (0, 255, 0), 3)

plt.subplot(1, 2, 2)  # filas, cloumnas, índice de celda
plt.imshow(image, cmap='gray', interpolation='bicubic')
plt.title("Contornos")
plt.xticks([]), plt.yticks([])

plt.show()

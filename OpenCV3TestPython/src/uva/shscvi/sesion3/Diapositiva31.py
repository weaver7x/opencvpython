# -*- coding: UTF-8 -*-

'''
Sesión 3
Diapositiva 31
Detección de rectas de una imagen binarizada (desde Canny o umbralizado)
--> Transformada de Hough
Created on 11/11/2014

@author: Cristian TG
'''

import cv2
import FuncionesComunes
import numpy as np
from matplotlib import pyplot as plt

# Opción 1
orig = FuncionesComunes.imgRGB('pcuva.bmp', 1)
gray = cv2.cvtColor(FuncionesComunes.imgBGR('pcuva.bmp', 1), cv2.COLOR_BGR2GRAY)

# Opción 2 - Escala de grises
# img = cv2.imread('coche.png', 0)

# 1 Canny
# Opcional suavizado blur
# gray = cv2.GaussianBlur(gray, (5, 5), 0)
# Obligatorio: Canny
edges = cv2.Canny(gray, 50, 150, apertureSize=3)  # 100 mínimo y 200 máximo para la histéresis
plt.subplot(1, 3, 1)  # filas, cloumnas, índice de celda
plt.imshow(edges, cmap='gray', interpolation='bicubic')
plt.title("Canny")
plt.xticks([]), plt.yticks([])

# cv2.HoughLines(par1,par2,par3)
# par1: imagen origen binarizada mediante umbralizado o Canny
# par2: precisión en el parametro phi y thita
# par3: longitud mínima del segmento para considerarlo una línea
lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)
for line in lines:
    rho, theta = line[0]

    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a * rho
    y0 = b * rho

    x1 = int(x0 + 1000 * (-b))
    y1 = int(y0 + 1000 * (a))
    x2 = int(x0 - 1000 * (-b))
    y2 = int(y0 - 1000 * (a))

    # últimos parámetros: color y grosor
    cv2.line(orig, (x1, y1), (x2, y2), (255, 0, 0), 4)

plt.subplot(1, 3, 2)  # filas, cloumnas, índice de celda
plt.imshow(orig, cmap='gray', interpolation='bicubic')
plt.title("Hough")
plt.xticks([]), plt.yticks([])
   

orig = FuncionesComunes.imgRGB('pcuva.bmp', 1)
# cv2.HoughLinesP(image, rho, theta, threshold[, lines[, minLineLength[, maxLineGap]]]) → lines
# Parameters:    
# image – 8-bit, single-channel binary source image. The image may be modified by the function.
# lines – Output vector of lines. Each line is represented by a 4-element vector  (x_1, y_1, x_2, y_2) , where (x_1,y_1) and  (x_2, y_2) are # the ending points of each detected line segment.
# rho – Distance resolution of the accumulator in pixels.
# theta – Angle resolution of the accumulator in radians.
# threshold – Accumulator threshold parameter. Only those lines are returned that get enough votes ( >\texttt{threshold} ).
# minLineLength – Minimum line length. Line segments shorter than that are rejected.
# maxLineGap – Maximum allowed gap between points on the same line to link them.
lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, 100, 10)
for x1, y1, x2, y2 in lines[0]:
    cv2.line(orig, (x1, y1), (x2, y2), (255, 0, 0), 4)

plt.subplot(1, 3, 3)  # filas, cloumnas, índice de celda
plt.imshow(orig, cmap='gray', interpolation='bicubic')
plt.title("HoughP")
plt.xticks([]), plt.yticks([])

plt.show()

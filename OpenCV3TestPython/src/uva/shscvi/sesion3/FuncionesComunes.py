# -*- coding: UTF-8 -*-

'''
Sesión 3
Funciones comunes
Created on 11/11/2014

@author: Cristian TG
'''

import cv2

def imgRGB(imgRuta, modo):
    '''MatPlotLib trabaja con RGB - Poco eficiente
    imgRuta ruta de la imagen
    modo 0 en grises, 1 en color sin transparencia, -1 en color con transparencia'''
    img = cv2.imread(imgRuta, modo)
    b, g, r = cv2.split(img)       
    return  cv2.merge((r, g, b))

def imgBGR(imgRuta, modo):
    '''OpenCV trabaja con RGB - Eficiente
    imgRuta ruta de la imagen
    modo 0 en grises, 1 en color sin transparencia, -1 en color con transparencia'''        
    return cv2.imread(imgRuta, modo)
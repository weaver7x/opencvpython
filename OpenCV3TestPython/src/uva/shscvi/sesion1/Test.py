# -*- coding: UTF-8 -*-


'''
Sesión 1
Leer y mostrar una imagen con OpenCV
Created on 14/10/2014

@author: Cristian TG
'''
import cv2
from cv2 import waitKey


# 0 en escala de grises, 1 en color sin transparencia, -1
# Lee la imagen en BGR
imgBGR = cv2.imread("./android.png", 1)
cv2.imshow("Mi primer ejemplo Python OpenCV", imgBGR)
waitKey(0)  # Para mostrar la ventana
cv2.destroyAllWindows()  # Destruye todas las ventanas creadas
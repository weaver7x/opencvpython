# SHSCVI_Individual_1415_P5

Algoritmo de Roberts. Estudiar el algoritmo de Roberts para la detección de
bordes:
¿Qué es el tamaño del entorno (tamaño de la máscara)? Razonad la elección
 del tamaño de la máscara.
¿Cuál son las ventajas y desventajas de éste método en relación a otros 
métodos diferenciales?
¿Cuándo se trabaja con éste método y por qué normalmente se calcula la 
magnitud del gradiente y no la dirección?
Nota: como extra se comparará con otros filtros lineales y con el filtro de Canny.


Sistemas Hardware y Software de Captura y Visualización de Imagen, 
Máster en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.


# Autor

Tejedor García,		Cristian.


# Estructura de directorios:
/doc/
Documentación del proyecto

/src/
Código fuente de la aplicación

/img/
Imágenes de prueba de la aplicación

/resultados/
Imágenes y pdfs obtenidos tras ejecutar
la aplicación


# Intérprete de Python utilizado
2.7.X
https://www.python.org/downloads/


# Versión de OpenCV utilizada
3.0.0-alpha
http://sourceforge.net/projects/opencvlibrary/files/


# Dependencias
Numpy 1.8.2 http://sourceforge.net/projects/numpy/files/NumPy/

Matplotlib 1.4.2 http://matplotlib.org/downloads.html


# Clase principal
src/Practica5.py